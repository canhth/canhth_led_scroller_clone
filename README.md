# led_scroller_clone

Link app clone: [Led scroller](https://play.google.com/store/apps/details?id=oops.ledscroller)

Simple Electronic Displays App. Scrolling text is so easy.

## Version

- Flutter 3.10.5

## Command

- dart run build_runner build