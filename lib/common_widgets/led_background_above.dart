import 'package:flutter/material.dart';

class LedBackgroundAbove extends StatelessWidget {
  const LedBackgroundAbove({
    super.key,
    required this.imagePath,
  });

  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Image.asset(
        imagePath,
        fit: BoxFit.cover,
      ),
    );
  }
}
