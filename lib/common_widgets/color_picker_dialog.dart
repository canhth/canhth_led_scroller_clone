import 'dart:math';

import 'package:flutter/material.dart';

class ColorPickerDialog extends StatelessWidget {
  const ColorPickerDialog({
    super.key,
    required this.title,
    required this.onChanged,
    required this.colorList,
  });

  final String title;
  final List<Color> colorList;
  final Function(Color) onChanged;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.black,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2),
        side: const BorderSide(color: Colors.grey, width: 2),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 12.0,
                horizontal: 8.0,
              ),
              child: Text(
                title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
                textAlign: TextAlign.left,
              ),
            ),
            GridView.builder(
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: min(colorList.length, 6),
              ),
              physics: const NeverScrollableScrollPhysics(),
              itemCount: colorList.length,
              itemBuilder: (BuildContext context, int index) {
                final color = colorList[index];

                return GestureDetector(
                  onTap: () {
                    onChanged((color));
                  },
                  child: Container(
                    color: color,
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
