import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../pages/home/bloc/home_cubit.dart';
import '../utils/enums/sliding_state_enum.dart';

class SlidingText extends StatefulWidget {
  const SlidingText({
    super.key,
    required this.text,
    required this.textStyle,
    required this.speed,
    this.slidingState = SlidingState.leftToRight,
  });

  final String text;
  final TextStyle textStyle;
  final double speed;

  final SlidingState slidingState;

  @override
  State<SlidingText> createState() => _SlidingTextState();
}

class _SlidingTextState extends State<SlidingText>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  Duration durationFromSpeed(double value) {
    return Duration(milliseconds: 1500 ~/ value);
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: durationFromSpeed(widget.speed),
    );

    _animation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.linear),
    );

    _animationController
      ..forward()
      ..repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<HomeCubit, HomeState>(
          listener: (context, state) {
            if (state.slidingState.isStop) {
              _animationController
                ..stop()
                ..reset();
            } else {
              _animationController
                ..forward()
                ..repeat();
            }
          },
          listenWhen: (previous, current) {
            return previous.slidingState != current.slidingState;
          },
        ),
        BlocListener<HomeCubit, HomeState>(
          listener: (context, state) {
            _animationController.duration = durationFromSpeed(state.speed);

            if (_animationController.isAnimating) {
              _animationController
                ..forward()
                ..repeat();
            }
          },
          listenWhen: (previous, current) {
            return previous.speed != current.speed;
          },
        ),
      ],
      child: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return CustomPaint(
            size: const Size(double.maxFinite, double.maxFinite),
            painter: _SlidingTextPainter(
              text: widget.text,
              textStyle: widget.textStyle,
              slidingState: widget.slidingState,
              animationValue: _animation.value,
            ),
          );
        },
      ),
    );
  }
}

class _SlidingTextPainter extends CustomPainter {
  final String text;
  final TextStyle textStyle;
  final SlidingState slidingState;
  final double animationValue;

  _SlidingTextPainter({
    required this.text,
    required this.textStyle,
    required this.slidingState,
    required this.animationValue,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final textPainter = TextPainter(
      text: TextSpan(
        text: text,
        style: textStyle,
      ),
      textDirection: TextDirection.ltr,
    );

    textPainter.layout();

    final textWidth = textPainter.width;
    final textHeight = textPainter.height;

    final sizeWidth = size.width;
    final sizeHeight = size.height;

    double dx = 10.0;
    final dy = (sizeHeight - textHeight) / 2;

    if (animationValue == 0.0) {
      if (textWidth < sizeWidth) {
        dx = (sizeWidth - textWidth) / 2;
      }
    } else {
      if (slidingState.isRightToLeft) {
        dx = sizeWidth + (-1) * (sizeWidth + textWidth) * animationValue;
      }
      //
      else if (slidingState.isLeftToRight) {
        dx = -textWidth + (sizeWidth + textWidth) * animationValue;
      }
    }

    final offset = Offset(dx, dy);

    textPainter.paint(canvas, offset);
  }

  @override
  bool shouldRepaint(_SlidingTextPainter oldDelegate) {
    return text != oldDelegate.text ||
        textStyle != oldDelegate.textStyle ||
        slidingState != oldDelegate.slidingState ||
        animationValue != oldDelegate.animationValue;
  }
}
