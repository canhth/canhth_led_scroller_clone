import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common_widgets/common_widgets.dart';
import '../../constants/app_constants.dart';
import '../../gen/assets.gen.dart';
import '../home/bloc/home_cubit.dart';

class HorizontalLedPage extends StatefulWidget {
  const HorizontalLedPage({super.key});

  @override
  State<HorizontalLedPage> createState() => _HorizontalLedPageState();
}

class _HorizontalLedPageState extends State<HorizontalLedPage> {
  @override
  void initState() {
    super.initState();
    setLandscapeOrientation();
  }

  @override
  void dispose() {
    setPortraitOrientation();
    super.dispose();
  }

  void setLandscapeOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  void setPortraitOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.backgroundColor != current.backgroundColor;
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: state.backgroundColor,
          body: SizedBox.expand(
            child: Stack(
              fit: StackFit.expand,
              children: [
                const _TextDisplay(),
                LedBackgroundAbove(
                  imagePath: Assets.images.gridLarge.path,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class _TextDisplay extends StatelessWidget {
  const _TextDisplay({super.key});

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        final fontSize =
            state.fontSize * screenHeight / AppConstants.previewHeight;

        return BlinkAnimation(
          enable: state.isBlinkingText,
          child: SlidingText(
            text: state.textDisplay,
            textStyle: TextStyle(
              fontSize: fontSize,
              color: state.textColor,
              fontWeight: FontWeight.bold,
            ),
            speed: state.speed,
            slidingState: state.slidingState,
          ),
        );
      },
    );
  }
}
