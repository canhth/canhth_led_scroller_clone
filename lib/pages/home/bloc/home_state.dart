part of 'home_cubit.dart';

class HomeState {
  final String textDisplay;
  final SlidingState slidingState;
  // range: (20.0 - 200.0)
  final double fontSize;
  // range: (0.0 - 1.0)
  final double speed;
  final FontSizeAdjustment fontSizeAdjustment;
  final bool showNeonLightStartButton;
  final bool isBlinkingText;
  final Color backgroundColor;
  final Color textColor;

  double get minFontSize => 20.0;
  double get maxFontSize => 200.0;

  double get minSpeed => 0.0;
  double get maxSpeed => 1.0;

  const HomeState({
    required this.textDisplay,
    required this.slidingState,
    required this.fontSize,
    required this.speed,
    required this.fontSizeAdjustment,
    required this.showNeonLightStartButton,
    required this.isBlinkingText,
    required this.backgroundColor,
    required this.textColor,
  });

  factory HomeState.init() => HomeState(
        textDisplay: '',
        slidingState: SlidingState.stop,
        fontSize: 100,
        speed: 0.5,
        fontSizeAdjustment: FontSizeAdjustment.none,
        showNeonLightStartButton: false,
        isBlinkingText: false,
        backgroundColor: const Color(0xff292929),
        textColor: Colors.green.shade800,
      );

  HomeState copyWith({
    String? textDisplay,
    SlidingState? slidingState,
    double? fontSize,
    double? speed,
    bool? isStopSliding,
    FontSizeAdjustment? fontSizeAdjustment,
    bool? showNeonLightStartButton,
    bool? isBlinkingText,
    Color? backgroundColor,
    Color? textColor,
  }) {
    return HomeState(
      textDisplay: textDisplay ?? this.textDisplay,
      slidingState: slidingState ?? this.slidingState,
      fontSize: fontSize ?? this.fontSize,
      speed: speed ?? this.speed,
      fontSizeAdjustment: fontSizeAdjustment ?? this.fontSizeAdjustment,
      showNeonLightStartButton:
          showNeonLightStartButton ?? this.showNeonLightStartButton,
      isBlinkingText: isBlinkingText ?? this.isBlinkingText,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      textColor: textColor ?? this.textColor,
    );
  }
}
