import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/enums/font_size_adjustment_enum.dart';
import '../../../utils/enums/sliding_state_enum.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit(super.initialState);

  void decreaseFontSize() {
    final fontSize = state.fontSize;
    if (fontSize > state.minFontSize) {
      emit(state.copyWith(fontSize: fontSize - 4));
    }
  }

  void increaseFontSize() {
    final fontSize = state.fontSize;
    if (fontSize < state.maxFontSize) {
      emit(state.copyWith(fontSize: fontSize + 4));
    }
  }

  void changeTextDisplay(String value) {
    emit(state.copyWith(textDisplay: value));
  }

  void changeSlidingState(SlidingState value) {
    emit(state.copyWith(slidingState: value));
  }

  void changeSpeed(double value) {
    emit(state.copyWith(speed: value));
  }

  void changeFontSizeAdjustment(FontSizeAdjustment value) {
    emit(state.copyWith(fontSizeAdjustment: value));
  }

  void changeShowNeonLightStartButton(bool value) {
    emit(state.copyWith(showNeonLightStartButton: value));
  }

  void toggleIsBlinkingText() {
    final newValue = !state.isBlinkingText;
    emit(state.copyWith(isBlinkingText: newValue));
  }

  void changeBackgroundColor(Color value) {
    emit(state.copyWith(backgroundColor: value));
  }

  void changeTextColor(Color value) {
    emit(state.copyWith(textColor: value));
  }
}
