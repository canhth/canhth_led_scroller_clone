import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common_widgets/common_widgets.dart';
import 'bloc/home_cubit.dart';
import 'widgets/widgets.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return DismissKeyboard(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: SingleChildScrollView(
            child: Column(
              children: [
                const LedDisplayPreview(),
                InputTextField(
                  onChanged: (value) {
                    final newValue = value.trim().replaceAll('\n', ' ');
                    context.read<HomeCubit>().changeTextDisplay(newValue);
                  },
                ),
                const SizedBox(height: 30),
                GridView(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    childAspectRatio: 1 / 1.2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  shrinkWrap: true,
                  children: const [
                    LeftSlideButton(),
                    RightSlideButton(),
                    StopButton(),
                    DecreaseFontSizeButton(),
                    IncreaseFontSizeButton(),
                  ],
                ),
                const SizedBox(height: 20),
                const Row(
                  children: [
                    SizedBox(width: 10),
                    TextColorButton(),
                    SizedBox(width: 10),
                    BlinkButton(),
                    SizedBox(width: 10),
                    Expanded(
                      child: SpeedSlider(),
                    ),
                    SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 20),
                const Row(
                  children: [
                    SizedBox(width: 10),
                    BackColorButton(),
                    SizedBox(width: 10),
                    Expanded(child: SizedBox()),
                    Expanded(
                      flex: 2,
                      child: StartButton(),
                    ),
                    Expanded(child: SizedBox()),
                    MoreInfoButton(),
                    SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 40),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
