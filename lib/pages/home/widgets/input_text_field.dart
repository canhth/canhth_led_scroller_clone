import 'package:flutter/material.dart';

class InputTextField extends StatefulWidget {
  const InputTextField({
    super.key,
    required this.onChanged,
  });

  final Function(String) onChanged;

  @override
  State<InputTextField> createState() => _InputTextFieldState();
}

class _InputTextFieldState extends State<InputTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: TextField(
        minLines: 1,
        maxLines: 2,
        keyboardType: TextInputType.multiline,
        textAlignVertical: TextAlignVertical.center,
        decoration: const InputDecoration(
          hintText: 'Enter your message here!',
          fillColor: Colors.white,
          filled: true,
          contentPadding: EdgeInsets.symmetric(horizontal: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 2.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.deepOrange, width: 2.0),
          ),
        ),
        onChanged: widget.onChanged,
      ),
    );
  }
}
