import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../full_screen_horizontal_led/horizontal_led_page.dart';
import '../bloc/home_cubit.dart';
import 'neon_text_button.dart';

class StartButton extends StatelessWidget {
  const StartButton({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final buttonHeight = (screenSize.width - 6 * 10) / 5 * 1.2;

    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.showNeonLightStartButton !=
            current.showNeonLightStartButton;
      },
      builder: (context, state) {
        return NeonTextButton(
          text: 'start',
          onPressed: () {
            context.read<HomeCubit>().changeShowNeonLightStartButton(true);
            Future.delayed(
              const Duration(milliseconds: 20),
              () {
                context.read<HomeCubit>().changeShowNeonLightStartButton(false);
              },
            );
          },
          height: buttonHeight,
          showNeonLight: state.showNeonLightStartButton,
          onTapDown: (p0) {
            FocusManager.instance.primaryFocus?.unfocus();
            context.read<HomeCubit>().changeShowNeonLightStartButton(true);
          },
          onTapUp: (p0) async {
            context.read<HomeCubit>().changeShowNeonLightStartButton(false);
            await _navigateToHorizontalLedPage(context);
            setPortraitOrientation();
          },
        );
      },
    );
  }

  Future<void> _navigateToHorizontalLedPage(BuildContext context) async {
    await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => const HorizontalLedPage(),
    ));
  }

  void setPortraitOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
