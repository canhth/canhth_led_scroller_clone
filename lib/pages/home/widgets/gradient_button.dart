import 'package:flutter/material.dart';

class GradientTextButton extends StatelessWidget {
  const GradientTextButton({
    super.key,
    required this.text,
    required this.onPressed,
    this.textStyle = const TextStyle(
      fontSize: 20,
      color: Colors.black,
    ),
    this.width,
    this.height,
    this.onTapDown,
    this.onTapUp,
    this.gradient,
  });

  final String text;
  final double? width;
  final double? height;
  final VoidCallback onPressed;
  // Start press
  final Function(TapDownDetails)? onTapDown;
  // End press
  final Function(TapUpDetails)? onTapUp;

  final TextStyle textStyle;
  final Gradient? gradient;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          gradient: gradient ??
              LinearGradient(
                colors: [
                  Colors.purpleAccent.shade100,
                  Colors.greenAccent.shade100,
                  Colors.yellowAccent.shade100,
                  Colors.purpleAccent.shade100,
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
          borderRadius: BorderRadius.circular(16),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.all(4),
        child: FittedBox(
          child: Text(
            text.toUpperCase(),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: textStyle.copyWith(
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
