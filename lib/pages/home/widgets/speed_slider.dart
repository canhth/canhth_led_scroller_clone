import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/home_cubit.dart';

class SpeedSlider extends StatelessWidget {
  const SpeedSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.speed != current.speed;
      },
      builder: (context, state) {
        return SliderTheme(
          data: SliderThemeData(
            trackHeight: 20,
            thumbShape: _RoundedRectangleRangeSliderThumbShape(
              thumbSize: const Size(16, 30),
              borderRadius: 4,
              color: Colors.grey.shade400,
            ),
            trackShape: const _CustomTrackShape(
              borderRadius: 4,
            ),
          ),
          child: Slider(
            value: state.speed,
            min: state.minSpeed,
            max: state.maxSpeed,
            onChanged: (value) {
              context.read<HomeCubit>().changeSpeed(value);
            },
            activeColor: Colors.yellow.shade800,
            inactiveColor: Colors.grey.shade600,
          ),
        );
      },
    );
  }
}

class _RoundedRectangleRangeSliderThumbShape extends SliderComponentShape {
  final Size thumbSize;
  final double borderRadius;
  final Color? color;

  const _RoundedRectangleRangeSliderThumbShape({
    this.color,
    required this.thumbSize,
    required this.borderRadius,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return thumbSize;
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {
    final canvas = context.canvas;
    final rect = Rect.fromCenter(
      center: center,
      width: thumbSize.width,
      height: thumbSize.height,
    );
    final thumbRadius = borderRadius;
    final thumbPaint = Paint()
      ..color = color ?? sliderTheme.thumbColor!
      ..style = PaintingStyle.fill;

    canvas.drawRRect(
      RRect.fromRectAndRadius(rect, Radius.circular(thumbRadius)),
      thumbPaint,
    );
  }
}

class _CustomTrackShape extends SliderTrackShape {
  final double borderRadius;

  const _CustomTrackShape({
    required this.borderRadius,
  });

  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final trackHeight = sliderTheme.trackHeight;
    final trackLeft = offset.dx;
    final trackTop = offset.dy + (parentBox.size.height - trackHeight!) / 2.0;
    final trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }

  @override
  void paint(
    PaintingContext context,
    Offset offset, {
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required Animation<double> enableAnimation,
    required Offset thumbCenter,
    Offset? secondaryOffset,
    bool isEnabled = false,
    bool isDiscrete = false,
    required TextDirection textDirection,
  }) {
    final trackRect = getPreferredRect(
      parentBox: parentBox,
      offset: offset,
      sliderTheme: sliderTheme,
      isEnabled: isEnabled,
      isDiscrete: isDiscrete,
    );

    final activeTrackPaint = Paint()..color = sliderTheme.activeTrackColor!;

    final inactiveTrackPaint = Paint()..color = sliderTheme.inactiveTrackColor!;

    context.canvas.drawRRect(
      RRect.fromRectAndRadius(
        trackRect,
        Radius.circular(borderRadius),
      ),
      inactiveTrackPaint,
    );

    final thumbRadius =
        sliderTheme.thumbShape!.getPreferredSize(isEnabled, isDiscrete).width /
            2.0;
    final activeTrackSegment = Rect.fromLTRB(
      trackRect.left,
      trackRect.top,
      thumbCenter.dx - thumbRadius,
      trackRect.bottom,
    );

    context.canvas.drawRRect(
      RRect.fromRectAndRadius(
        activeTrackSegment,
        Radius.circular(borderRadius),
      ),
      activeTrackPaint,
    );
  }
}
