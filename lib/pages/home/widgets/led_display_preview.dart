import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common_widgets/blink_animation.dart';
import '../../../common_widgets/led_background_above.dart';
import '../../../common_widgets/sliding_text.dart';
import '../../../constants/app_constants.dart';
import '../../../gen/assets.gen.dart';
import '../bloc/home_cubit.dart';

class LedDisplayPreview extends StatelessWidget {
  const LedDisplayPreview({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.backgroundColor != current.backgroundColor;
      },
      builder: (context, state) {
        return Container(
          height: AppConstants.previewHeight,
          margin: const EdgeInsets.symmetric(vertical: 50),
          color: state.backgroundColor,
          alignment: Alignment.center,
          child: Stack(
            alignment: Alignment.center,
            children: [
              const Positioned.fill(
                child: _TextDisplay(),
              ),
              Positioned.fill(
                child: LedBackgroundAbove(
                  imagePath: Assets.images.gridMedium.path,
                ),
              ),
              _buildHorizontalBarOnTop(),
              Positioned(
                right: 0,
                top: -4,
                child: _buildPreviewLabel(),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildHorizontalBarOnTop() {
    return const Align(
      alignment: Alignment.topCenter,
      child: Divider(
        height: 4,
        thickness: 4,
        color: Colors.grey,
      ),
    );
  }

  Widget _buildPreviewLabel() {
    return const Text(
      'Preview',
      style: TextStyle(
        color: Colors.yellow,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}

class _TextDisplay extends StatelessWidget {
  const _TextDisplay({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        return BlinkAnimation(
          enable: state.isBlinkingText,
          child: SlidingText(
            text: state.textDisplay,
            textStyle: TextStyle(
              fontSize: state.fontSize,
              color: state.textColor,
              fontWeight: FontWeight.bold,
            ),
            speed: state.speed,
            slidingState: state.slidingState,
          ),
        );
      },
    );
  }
}
