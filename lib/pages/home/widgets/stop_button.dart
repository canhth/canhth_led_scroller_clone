import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/enums/sliding_state_enum.dart';
import '../bloc/home_cubit.dart';
import 'neon_text_button.dart';

class StopButton extends StatelessWidget {
  const StopButton({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final buttonWidth = (screenSize.width - 6 * 10) / 5;

    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.slidingState != current.slidingState;
      },
      builder: (context, state) {
        return NeonTextButton(
          text: 'Stop',
          showNeonLight: state.slidingState.isStop,
          onPressed: () {
            context.read<HomeCubit>().changeSlidingState(SlidingState.stop);
          },
          width: buttonWidth,
          height: buttonWidth * 1.2,
        );
      },
    );
  }
}
