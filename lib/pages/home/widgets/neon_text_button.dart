import 'package:flutter/material.dart';

class NeonTextButton extends StatelessWidget {
  const NeonTextButton({
    super.key,
    required this.text,
    required this.onPressed,
    this.showNeonLight = false,
    this.textStyle = const TextStyle(
      fontSize: 20,
      color: Colors.black,
    ),
    this.width,
    this.height,
    this.onTapDown,
    this.onTapUp,
  });

  final String text;
  final double? width;
  final double? height;
  final bool showNeonLight;
  final TextStyle textStyle;

  final VoidCallback onPressed;
  // Start press
  final Function(TapDownDetails)? onTapDown;
  // End press
  final Function(TapUpDetails)? onTapUp;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          gradient: showNeonLight
              ? null
              : LinearGradient(
                  colors: [
                    Colors.grey.shade600,
                    Colors.white,
                  ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
          boxShadow: showNeonLight
              ? const [
                  BoxShadow(
                    color: Colors.lightBlue,
                  ),
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: -4.0,
                    blurRadius: 4.0,
                  )
                ]
              : null,
          borderRadius: BorderRadius.circular(16),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.all(4),
        child: FittedBox(
          child: Text(
            text.toUpperCase(),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: textStyle.copyWith(
              fontWeight: FontWeight.w800,
              color: showNeonLight ? Colors.lightBlue : Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}
