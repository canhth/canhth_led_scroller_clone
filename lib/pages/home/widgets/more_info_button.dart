import 'package:flutter/material.dart';

class MoreInfoButton extends StatelessWidget {
  const MoreInfoButton({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        showDialog(
          context: context,
          builder: (ctx) {
            return const _Dialog();
          },
        );
      },
      child: const Icon(
        Icons.more_vert,
        color: Colors.white,
        size: 32,
      ),
    );
  }
}

class _Dialog extends StatefulWidget {
  const _Dialog({super.key});

  @override
  State<_Dialog> createState() => _DialogState();
}

class _DialogState extends State<_Dialog> {
  bool isPressed = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: isPressed ? Colors.orange : Colors.white,
      child: GestureDetector(
        onTapDown: (details) {
          setState(() {
            isPressed = true;
          });
        },
        onTapUp: (details) {
          setState(() {
            isPressed = false;
          });
        },
        onTap: () {},
        child: const Padding(
          padding: EdgeInsets.only(bottom: 20, left: 8),
          child: Text(
            'Terms of Service',
            style: TextStyle(
              fontSize: 28,
              color: Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}
