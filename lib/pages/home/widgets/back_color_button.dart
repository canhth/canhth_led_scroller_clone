import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common_widgets/color_picker_dialog.dart';
import '../../../constants/app_constants.dart';
import '../bloc/home_cubit.dart';
import 'gradient_button.dart';

class BackColorButton extends StatelessWidget {
  const BackColorButton({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final buttonWidth = (screenSize.width - 6 * 10) / 5;

    return GradientTextButton(
      text: 'Back\ncolor',
      onPressed: () {
        showDialog(
          context: context,
          builder: (ctx) {
            return ColorPickerDialog(
              title: 'Background Color Picker',
              onChanged: (value) {
                context.read<HomeCubit>().changeBackgroundColor(value);
                Navigator.of(ctx).pop();
              },
              colorList: AppConstants.backColorList,
            );
          },
        );
      },
      width: buttonWidth,
      height: buttonWidth * 1.2,
    );
  }
}
