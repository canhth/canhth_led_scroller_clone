import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../gen/assets.gen.dart';
import '../../../utils/enums/sliding_state_enum.dart';
import '../bloc/home_cubit.dart';
import 'neon_icon_button.dart';

class LeftSlideButton extends StatelessWidget {
  const LeftSlideButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.slidingState != current.slidingState;
      },
      builder: (context, state) {
        return NeonIconButton(
          icon: SvgPicture.asset(Assets.icons.leftArrow),
          neonIcon: SvgPicture.asset(
            Assets.icons.leftArrow,
            color: Colors.lightBlue,
          ),
          onPressed: () {
            context
                .read<HomeCubit>()
                .changeSlidingState(SlidingState.rightToLeft);
          },
          showNeonLight: state.slidingState.isRightToLeft,
        );
      },
    );
  }
}
