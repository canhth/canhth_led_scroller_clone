import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common_widgets/color_picker_dialog.dart';
import '../bloc/home_cubit.dart';
import 'gradient_button.dart';

class TextColorButton extends StatelessWidget {
  const TextColorButton({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final buttonWidth = (screenSize.width - 6 * 10) / 5;

    return GradientTextButton(
      text: 'Text\ncolor',
      onPressed: () {
        showDialog(
          context: context,
          builder: (ctx) {
            return ColorPickerDialog(
              title: 'Background Color Picker',
              onChanged: (value) {
                context.read<HomeCubit>().changeTextColor(value);
                Navigator.of(ctx).pop();
              },
              colorList: const [...Colors.primaries, ...Colors.accents],
            );
          },
        );
      },
      width: buttonWidth,
      height: buttonWidth * 1.2,
    );
  }
}
