import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/home_cubit.dart';
import 'neon_text_button.dart';

class BlinkButton extends StatelessWidget {
  const BlinkButton({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final buttonWidth = (screenSize.width - 6 * 10) / 5;

    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.isBlinkingText != current.isBlinkingText;
      },
      builder: (context, state) {
        return NeonTextButton(
          text: 'Blink',
          onPressed: () {
            context.read<HomeCubit>().toggleIsBlinkingText();
          },
          showNeonLight: state.isBlinkingText,
          width: buttonWidth,
          height: buttonWidth * 1.2,
        );
      },
    );
  }
}
