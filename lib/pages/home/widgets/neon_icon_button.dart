import 'package:flutter/material.dart';

class NeonIconButton extends StatelessWidget {
  const NeonIconButton({
    super.key,
    required this.icon,
    required this.neonIcon,
    required this.onPressed,
    this.showNeonLight = false,
    this.width,
    this.height,
    this.onTapDown,
    this.onTapUp,
  });

  final Widget icon;
  final Widget neonIcon;
  final double? width;
  final double? height;
  final VoidCallback onPressed;
  // Start press
  final Function(TapDownDetails)? onTapDown;
  // End press
  final Function(TapUpDetails)? onTapUp;

  final bool showNeonLight;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          gradient: showNeonLight
              ? null
              : LinearGradient(
                  colors: [
                    Colors.grey.shade600,
                    Colors.white,
                  ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
          boxShadow: showNeonLight
              ? const [
                  BoxShadow(
                    color: Colors.lightBlue,
                  ),
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: -4.0,
                    blurRadius: 4.0,
                  )
                ]
              : null,
          borderRadius: BorderRadius.circular(16),
        ),
        alignment: Alignment.center,
        padding: const EdgeInsets.all(10),
        child: showNeonLight ? neonIcon : icon,
      ),
    );
  }
}
