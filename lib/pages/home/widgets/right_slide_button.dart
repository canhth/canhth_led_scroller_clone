import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../gen/assets.gen.dart';
import '../../../utils/enums/sliding_state_enum.dart';
import '../bloc/home_cubit.dart';
import 'neon_icon_button.dart';

class RightSlideButton extends StatelessWidget {
  const RightSlideButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.slidingState != current.slidingState;
      },
      builder: (context, state) {
        return NeonIconButton(
          icon: SvgPicture.asset(Assets.icons.rightArrow),
          neonIcon: SvgPicture.asset(
            Assets.icons.rightArrow,
            color: Colors.lightBlue,
          ),
          onPressed: () {
            context
                .read<HomeCubit>()
                .changeSlidingState(SlidingState.leftToRight);
          },
          showNeonLight: state.slidingState.isLeftToRight,
        );
      },
    );
  }
}
