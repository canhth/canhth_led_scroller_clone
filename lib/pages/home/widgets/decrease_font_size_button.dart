import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../gen/assets.gen.dart';
import '../../../utils/enums/font_size_adjustment_enum.dart';
import '../bloc/home_cubit.dart';
import 'neon_icon_button.dart';

Timer? _timer;

class DecreaseFontSizeButton extends StatelessWidget {
  const DecreaseFontSizeButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        return previous.fontSizeAdjustment != current.fontSizeAdjustment;
      },
      builder: (context, state) {
        return NeonIconButton(
          icon: SvgPicture.asset(Assets.icons.minus),
          neonIcon: SvgPicture.asset(
            Assets.icons.minus,
            color: Colors.lightBlue,
          ),
          showNeonLight: state.fontSizeAdjustment.isDecreasing,
          onPressed: () {
            context
                .read<HomeCubit>()
                .changeFontSizeAdjustment(FontSizeAdjustment.decreasing);
            Future.delayed(
              const Duration(milliseconds: 20),
              () {
                context
                    .read<HomeCubit>()
                    .changeFontSizeAdjustment(FontSizeAdjustment.none);
              },
            );
            context.read<HomeCubit>().decreaseFontSize();
          },
          onTapDown: (p0) {
            context
                .read<HomeCubit>()
                .changeFontSizeAdjustment(FontSizeAdjustment.decreasing);

            _timer = Timer.periodic(const Duration(milliseconds: 50), (timer) {
              context.read<HomeCubit>().decreaseFontSize();
            });
          },
          onTapUp: (p0) {
            _timer?.cancel();
            context
                .read<HomeCubit>()
                .changeFontSizeAdjustment(FontSizeAdjustment.none);
          },
        );
      },
    );
  }
}
