enum SlidingState {
  rightToLeft,
  leftToRight,
  stop,
}

extension SlidingStateEx on SlidingState {
  bool get isRightToLeft => this == SlidingState.rightToLeft;
  bool get isLeftToRight => this == SlidingState.leftToRight;
  bool get isStop => this == SlidingState.stop;
}
