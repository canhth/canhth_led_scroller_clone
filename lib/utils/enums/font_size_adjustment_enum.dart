enum FontSizeAdjustment {
  increasing,
  decreasing,
  none,
}

extension FontSizeAdjustmentEx on FontSizeAdjustment {
  bool get isIncreasing => this == FontSizeAdjustment.increasing;
  bool get isDecreasing => this == FontSizeAdjustment.decreasing;
  bool get isNone => this == FontSizeAdjustment.none;
}
