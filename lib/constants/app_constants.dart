import 'package:flutter/material.dart';

@immutable
class AppConstants {
  const AppConstants._();

  static double previewHeight = 200.0;

  static List<Color> backColorList = const [
    Color(0xff350000),
    Color(0xff352d00),
    Color(0xff313500),
    Color(0xff003504),
    Color(0xff002e35),
    Color(0xff3e013f),
    Color(0xff000000),
    Color(0xff292929),
    Color(0xff393939),
    Color(0xff797979),
    Color(0xffcccccc),
    Color(0xffffffff),
  ];
}
